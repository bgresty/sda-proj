//
//  Wave Display GUI.h
//  SDA Proj
//
//  Created by Ben Gresty on 03/01/2015.
//
//

#ifndef __SDA_Proj__Wave_Display_GUI__
#define __SDA_Proj__Wave_Display_GUI__

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include "Wave Display.h"

class WaveGui : public Component

{
public:

    WaveGui(AudioOscilloscope& oscilliscope_);
    
    void resized();

    
private:
    AudioOscilloscope& oscilloscope;
    
};

#endif /* defined(__SDA_Proj__Wave_Display_GUI__) */
