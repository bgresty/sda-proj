#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../JuceLibraryCode/JuceHeader.h"
#include "Wave Display.h"
#include "Looper.h"

#define LOOPERS 10

class Audio :   public MidiInputCallback,
public AudioIODeviceCallback,
public Component
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    
    Looper& getLooper()
    {
        increment++;
        return looper[increment];
    }
    
    AudioOscilloscope& getOsc() { return audioOscilloscope; }
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
private:
    AudioDeviceManager audioDeviceManager;
    
    Looper looper[LOOPERS];
    AudioOscilloscope audioOscilloscope;
    
    CriticalSection sharedMemory;
    
    int increment;
    
};



#endif  // AUDIO_H_INCLUDED
