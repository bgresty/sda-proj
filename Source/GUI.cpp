#include "GUI.h"

LooperGui::LooperGui(Looper& looper_) : looper(looper_)

{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnTop | Button::ConnectedOnBottom);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnTop | Button::ConnectedOnBottom);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
    saveButton.setButtonText ("Save");
    saveButton.setConnectedEdges(Button::ConnectedOnTop | Button::ConnectedOnBottom);
    saveButton.setColour(TextButton::buttonColourId, Colours::blue);
    saveButton.setColour(TextButton::buttonOnColourId, Colours::black);
    addAndMakeVisible (&saveButton);
    saveButton.addListener (this);
    
    loadButton.setButtonText ("Load");
    loadButton.setConnectedEdges(Button::ConnectedOnTop | Button::ConnectedOnBottom);
    loadButton.setColour(TextButton::buttonColourId, Colours::green);
    loadButton.setColour(TextButton::buttonOnColourId, Colours::black);
    addAndMakeVisible (&loadButton);
    loadButton.addListener (this);
    
    nameLabel.setText("Looper 1", dontSendNotification);
    nameLabel.setEditable(true);
    addAndMakeVisible (&nameLabel);
    
    volumeSlider.setRange(-6.0, 0.0);
    volumeSlider.setSliderStyle(Slider::LinearVertical);
    volumeSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 35, 25);
    addAndMakeVisible(&volumeSlider);
    volumeSlider.addListener(this);
    
    panSlider.setRange(-1.0, 1.0);
    panSlider.setSliderStyle(Slider::RotaryHorizontalVerticalDrag);
    panSlider.setTextBoxStyle(Slider::TextBoxLeft, false, 50, 25);
    addAndMakeVisible(&panSlider);
    panSlider.addListener(this);
    
//    addAndMakeVisible (liveAudioScroller = new LiveScrollingAudioDisplay());
//    
//    getSharedAudioDeviceManager().addAudioCallback (liveAudioScroller);
    
}



void LooperGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
        
        if (looper.getRecordState() == true)
            looper.setRecordState(false);
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);
        
        looper.setPlayState(looper.getRecordState());
        playButton.setToggleState(looper.getRecordState(), dontSendNotification);
    }
    else if (button == &saveButton)
    {
        if (looper.getPlayState() == true)
            AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon,"Error", "Stop playback before trying to save", "OK", this);
        else
            looper.save();
    }
    
}

void LooperGui::sliderValueChanged(Slider* slider)
{
    if (slider == &volumeSlider)
    {
        volumeSlider.getValue();
    }
    
    else if (slider == &panSlider)
    {
        panSlider.getValue();
    }
}

void LooperGui::paint (Graphics& g)
{
//    const float midY = getHeight() * 0.5f;
//    int samplesAgo = (nextSample + numElementsInArray (samples) - 1);
//    
//    RectangleList<float> waveform;
//    waveform.ensureStorageAllocated ((int) numElementsInArray (samples));
//    
//    for (int x = jmin (getWidth(), (int) numElementsInArray (samples)); --x >= 0;)
//    {
//        const float sampleSize = midY * samples [samplesAgo-- % numElementsInArray (samples)];
//        waveform.addWithoutMerging (Rectangle<float> ((float) x, midY - sampleSize, 1.0f, sampleSize * 2.0f));
//    }
//    
//    g.setColour (Colours::lightgreen);
//    g.fillRectList (waveform);
}

void LooperGui::resized()
{
    nameLabel.setBounds(0, 0, 80, 40);
    
    playButton.setBounds (0, 40, 80, 40);
    recordButton.setBounds(0, 80, 80, 40);
    saveButton.setBounds(0, 120, 80, 40);
    loadButton.setBounds(0, 160, 80, 40);
    
    volumeSlider.setBounds(80, 40, 40, 160);
    panSlider.setBounds(0, 200, 120, 40);
    
//    liveAudioScroller->setBounds (0, 60, getWidth() - 16, 64);
    
}


//AudioDeviceManager& LooperGui::getSharedAudioDeviceManager()
//{
//    if (sharedAudioDeviceManager == nullptr)
//    {
//        sharedAudioDeviceManager = new AudioDeviceManager();
//        sharedAudioDeviceManager->initialise (2, 2, 0, true, String::empty, 0);
//    }
//    
//    return *sharedAudioDeviceManager;
//}


