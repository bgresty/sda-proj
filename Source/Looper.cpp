#include "Looper.h"

Looper::Looper()
{
    //initialise - not playing / recording
    playState = false;
    recordState = false;
    //position to the start of audioSampleBuffer
    bufferPosition = 0;
    //audioSampleBuffer contents to zero
    audioSampleBuffer.setSize (1, bufferSize);
    audioSampleBuffer.clear();
}

Looper::~Looper()
{
    
}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState;
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState;
}

float Looper::processSample (float input)
{
    float output = 0.f;
    if (playState == true)
    {
        //play
        audioSample = audioSampleBuffer.getWritePointer(0, bufferPosition);
        
        output = *audioSample;
                
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 8)) == 0)
            output += 0.25f;
        
        //record
        if (recordState == true)
            *audioSample = input;
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    return output;
}

int Looper::getBufferPosition() { return bufferPosition; }

void Looper::save()
{
    
    FileChooser chooser ("Please select a file...", File::getSpecialLocation (File::userDesktopDirectory), "*.wav");
    if (chooser.browseForFileToSave(true))
    {
        File file (chooser.getResult().withFileExtension (".wav"));
        OutputStream* outStream = file.createOutputStream();
        WavAudioFormat wavFormat;
        AudioFormatWriter* writer = wavFormat.createWriterFor (outStream, 44100, 1, 16, NULL, 0);
        writer->writeFromAudioSampleBuffer (audioSampleBuffer, 0, audioSampleBuffer.getNumSamples());
        delete writer;
    }
}