//
//  Wave Display GUI.cpp
//  SDA Proj
//
//  Created by Ben Gresty on 03/01/2015.
//
//

#include "Wave Display GUI.h"

WaveGui::WaveGui(AudioOscilloscope& oscilloscope) : oscilloscope(oscilloscope)
{
    addAndMakeVisible(&oscilloscope);
}


void WaveGui::resized()
{
    oscilloscope.setBounds(0, 0, getWidth(), 150);
}