/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) :
audio (audio_),
looperGui0 (audio_.getLooper()),
waveGui0(audio_.getOsc())

{
    setSize (500, 400);
    
    addAndMakeVisible(looperGui0);
    addAndMakeVisible(waveGui0);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    looperGui0.setBounds(0, 0, getWidth(), getHeight());
    waveGui0.setBounds(0, 250, getWidth(), 200);
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "Settings", "", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
    {
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
        menu.addItem(LooperPrefs, "Looper Preferences", true, false);
    }
    
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == SettingsMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
        
        if (menuItemID == LooperPrefs)
        {
            //            FilenameComponent ("Load", "Users/BenGresty/Desktop", true, true, true, false, false, true);
            
        }
    }
}

