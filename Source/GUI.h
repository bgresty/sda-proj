#ifndef H_LooperGui
#define H_LooperGui

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioLiveScrollingDisplay.h"
#include "Looper.h"

/**
 Gui for the looper class
 */
class LooperGui :   public Component,
public Button::Listener,
public Slider::Listener

{
public:
    /**
     constructor - receives a reference to a Looper object to control
     */
    LooperGui(Looper& looper_);
    
    //Button Listener
    void buttonClicked (Button* button);
    
    //Slider Listener
    void sliderValueChanged (Slider *slider) override;
    
    //Component
    void resized();
    
    void paint (Graphics& g) override;
    
//    static AudioDeviceManager& getSharedAudioDeviceManager();
    
private:
    Looper& looper;
    
    TextButton playButton;
    TextButton recordButton;
    TextButton saveButton;
    TextButton loadButton;
    
//    ScopedPointer<LiveScrollingAudioDisplay> liveAudioScroller;
//    static ScopedPointer<AudioDeviceManager> sharedAudioDeviceManager;
    
    Label nameLabel;
    
    Slider volumeSlider;
    Slider panSlider;
    
//    float samples[1024];
//    int nextSample, subSample;
//    float accumulator;
    
};

#endif
