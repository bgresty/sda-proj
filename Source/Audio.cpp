#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    increment = -1;
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOn())
    {
        
    }
    else if (message.isNoteOff())
    {
        
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
   
    sharedMemory.enter();
    while(numSamples--)
    {
        float output = 0.f;
        
        for (int i = 0; i < (LOOPERS - 1); ++i)
            output += looper[i].processSample (*inL);
        
//        audioOscilloscope.processBlock(inputChannelData[0], 1);
        
        for (int i = 0; i < numSamples; ++i)
        {
            float inputSample = 0;
            
            for (int chan = 0; chan < numInputChannels; ++chan)
                if (inputChannelData[chan] != nullptr)
                    inputSample += std::abs (inputChannelData[chan][i]);
            
            audioOscilloscope.addSample (3.0f * inputSample);
        }
        
        for (int j = 0; j < numOutputChannels; ++j)
            if (outputChannelData[j] != nullptr)
                zeromem (outputChannelData[j], sizeof (float) * (size_t) numSamples);
        
        
        
        *outL = output;
        *outR = output;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
    sharedMemory.exit();
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    //audioOscilloscope.clear();
}

void Audio::audioDeviceStopped()
{
    audioOscilloscope.clear();
}